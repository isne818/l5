#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using namespace std;

////////////////////////// PROTOTYPE FUNCTION //////////////////////////

void NumElement(vector<int>number,int randomNumber);	// Generate the number element
void SumofValue(vector<int>number,int randomNumber);	// Calculate sum of values
void HigestValue(vector<int>number,int randomNumber);	// Heighest Value display
void LowestValue(vector<int>number,int randomNumber);	// Lowest Value display
void Mean(vector<int>number,int randomNumber);			// Mean func.
void Median(vector<int>number,int randomNumber);		// Median func.
void Even_OddNum(vector<int> number,int randomNumber);	// Amount of Even and Odd numbers
int LowtoHigh(vector<int> number,int randomNumber);		// Sort and display the number form lowest to heighest number
void Mode(vector<int>number,int randomNumber);			// Mode func.

/////////////////////////////////////////////////////////////////////////



int main(){
	srand(time(0));
	vector<int> number;
	int randomNumber = 50+rand()%101; // Random amount of number between 50-150
	
	for(int i=0; i<randomNumber; i++){
		int value = rand()%101; // Random number 0-100
		number.push_back(value); 
	}
	
	NumElement(number,randomNumber);
	SumofValue(number,randomNumber);
	HigestValue(number,randomNumber);
	LowestValue(number,randomNumber);
	Mean(number,randomNumber);
	Median(number,randomNumber);
	Mode(number, randomNumber);
	Even_OddNum(number, randomNumber);
	LowtoHigh(number,randomNumber);
	
	return 0;
}



///////////////////////////////////// FUNCTION ZONE /////////////////////////////////////

void NumElement(vector<int>number,int randomNumber){ // Big O = 1
	cout << endl <<  " Number of all elements is : "<< randomNumber << endl << endl;
}
//----------------------------------------------------------------------------------------------------//
void SumofValue(vector<int>number,int randomNumber){	// Big O = n
	int sum;
	for(int i=0; i<randomNumber; i++){
		sum = sum+number[i];
	}
	cout << "Sum of all elements : " << sum << endl << endl;
}
//----------------------------------------------------------------------------------------------------//
void HigestValue(vector<int>number,int randomNumber){	// Big O = n^2
	int hold, swap;
	do{
		swap=0;
		for(int i=0; i<randomNumber;i++){
			if(number[i]>number[i+1]){
			hold=number[i];
			number[i]=number[i+1];
			number[i+1]=hold;
			swap=1;
			}
		}
	}while(swap==1);
	cout << "Highest Value : " << number.back() << endl << endl;
}
//----------------------------------------------------------------------------------------------------//
void LowestValue(vector<int>number,int randomNumber){	// Big O = n^2
	int hold, swap;
	do{
		swap=0;
		for(int i=0; i<randomNumber;i++){
			if(number[i]>number[i+1]){
			hold=number[i];
			number[i]=number[i+1];
			number[i+1]=hold;
			swap=1;
			}
		}
	}while(swap==1);
	cout << "Lowest Value : " << number.front() << endl << endl;
}
//----------------------------------------------------------------------------------------------------//
void Mean(vector<int>number,int randomNumber){	// Big O = n
	int mean,sum;
	for(int i=0; i<randomNumber; i++){
		sum=sum+number[i];
	}
	mean = sum/randomNumber;
	cout << "Mean : " << mean << endl << endl;
}
//----------------------------------------------------------------------------------------------------//
void Median(vector<int>number,int randomNumber){	// Big O = n^2
	int swap,hold;
	int median;
	do{
		swap=0;
		for(int i=0; i<randomNumber;i++){
			if(number[i]>number[i+1]){
			hold=number[i];
			number[i]=number[i+1];
			number[i+1]=hold;
			swap=1;
			}
		}
	}while(swap==1);
		median = number[(randomNumber+1)/2];
		cout << "Median : " << median << endl << endl;
}
//----------------------------------------------------------------------------------------------------//
void Even_OddNum(vector<int> number,int randomNumber){ // Big-O = n
	int even=0 ,odd=0;
	cout << "Even number : ";
	for(int i=0; i<randomNumber; i++){
		if(number[i]%2 == 0){
			even++;
		}
		else
		{
			odd++;
		}
	}cout << even << endl;
	cout << "Odd number : " << odd << endl;
}
//----------------------------------------------------------------------------------------------------//
int LowtoHigh(vector<int> number,int randomNumber){  // Big O = n
	int hold,swap;
	do{
		swap = 0;
		for(int i=0; i<randomNumber; i++){
			if(number[i] > number[i+1]){
				hold = number[i];
				number[i] = number[i+1];
				number[i+1] = hold;
				swap = 1;
			}
		}
	}while(swap==1);

	cout << "Value from lowest to highest is : " << endl;
	for(int i=0; i<randomNumber; i++){
		cout << setw(3) << number.at(i) << " ";
		if(i%20==0 && i!=0){
			cout << endl;
		}
	} cout << endl;
	return 0;
}
//----------------------------------------------------------------------------------------------------//
void Mode(vector<int>number,int randomNumber){  // Big O = n
	int mode,count=0,max=0,round=0;
	for(int i=0; i<randomNumber; i++){
		count=number.at(i);
		for(int j=0; j<randomNumber; j++){
			if(count==number.at(j)){
				round++;
				count==number.at(j);
			}
			if(round>max){
				max=round;
				mode = number.at(j);
			}
		}
		round = 0;
	}
	cout << "Mode : " << mode << endl << endl;
}